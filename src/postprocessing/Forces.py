import numpy as np
import pandas as pd

class Forces:
    """
    Forces Object
    Read OpenFOAM force file from path
    Return pandas DataFrame
    """
    def __init__(self, path, area = 2, isBinned = False):
        self._path = path        
        self.area = area
        if isBinned:
            self._data = self._readBinned()
        else:
            self._data = self._readforces()

    
    @property
    def data(self):
        return self._data
    
    @property
    def totalforces(self):
        return self._data[['total_x', 'total_y', 'total_z']]
    
    @property
    def pressureforces(self):
        return self._data[['pressure_x', 'pressure_y', 'pressure_z']]
    
    @property
    def viscousforces(self):
        return self._data[['viscous_x', 'viscous_y', 'viscous_z']]
    
    def renameforces(self, x_name="total_x", y_name="total_y", z_name="total_z"):
        
        return_data = self.data[["total_x", "total_y", "total_z"]]
        return return_data.rename(columns={
                                "total_x" : x_name, 
                                "total_y" : y_name, 
                                "total_z" : z_name})

    def _readforces(self):
        data = []

        try:
            with open(self._path, "r") as f:
                # read header
                for i in range(4):
                    header = [block.strip('('')') for block in f.readline().split()[1:]]
                    
                # read body, remove brackets
                for line in f:
                    tmp = [block.strip('('')') for block in line.split()]
                    data.append(tmp)
        except:
            print("No such file: ", self._path)
            return None
                
        # convert to numpy array, from string to float
        tmp_array = np.asarray(data, dtype=float)
        
        # non-dimensionalize
        # F* = F / (0.5 * rho * L * U**2)
        # With rho, L and U set to 1, F* = 2 * F

        # convert to pandas dataframe, using time indexing
        return pd.DataFrame(tmp_array[:, 1: ] / (0.5 * self.area), index=tmp_array[: , 0], columns = header[1:])

    def _readBinned(self):
        data = []

        with open(self._path, "r") as f:
            # read header

            for i in range(10):
                header = [block.strip('('')') for block in f.readline().split()[1:]]

                if "bins" in header:
                    nBins = header[-1]

            # read body, remove brackets
            for line in f:
                tmp = [block.strip('('')') for block in line.split()]
                data.append(tmp)

        iBins = int(nBins)

        # convert to numpy array, from string to float
        tmp_array = np.asarray(data[:], dtype=float)

        header_unique = ['total_x', 'total_y', 'total_z',
                         'pressure_x', 'pressure_y', 'pressure_z',
                         'viscous_x', 'viscous_y', 'viscous_z']

        binHeader = []

        for n in range(iBins):
            for force_name in header_unique:
                binHeader.append("{}_{}".format(force_name, n))

        # convert to pandas dataframe, using time indexing
        return pd.DataFrame(tmp_array[:, 1:], index=tmp_array[:, 0], columns=binHeader)

